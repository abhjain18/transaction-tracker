import { useState } from 'react';

export default function CashModal({ onSaveHandler, closeModal }) {
  const [partyName, setPartyName] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    onSaveHandler(partyName);
  };

  return (
    <div className="fixed inset-0 bg-gray-600 bg-opacity-75 overflow-y-auto h-full w-full flex items-center justify-center px-4">
      <div className="bg-white w-full max-w-md p-6 rounded-lg shadow-lg relative m-4">
        <button
          className="absolute top-3 right-3 text-gray-600 hover:text-gray-700"
          aria-label="close"
          onClick={closeModal}
        >
          <svg
            className="w-6 h-6"
            fill="none"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M6 18L18 6M6 6l12 12"
            ></path>
          </svg>
        </button>
        <form onSubmit={handleSubmit}>
          <h2 className="text-lg font-bold mb-4">Add Party</h2>
          <label
            htmlFor="party"
            className="block mb-2 text-sm font-medium text-gray-700"
          >
            Name
          </label>
          <input
            type="text"
            id="party"
            name="party"
            value={partyName}
            onChange={(e) => setPartyName(e.target.value)}
            className="border-gray-300 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 border rounded-md"
            required
          />
          <button
            type="submit"
            className="mt-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-full"
          >
            Save
          </button>
        </form>
      </div>
    </div>
  );
}
