'use client';

import React from 'react';
import Link from 'next/link';
function Users({ data }) {
  return (
    <div className="w-full overflow-x-auto">
      <table className="w-full min-w-full table-auto">
        <thead className="bg-gray-100">
          <tr>
            <th className="px-2 py-2 text-left text-xs font-bold text-gray-500 uppercase tracking-wider">
              Name
            </th>
            <th className="px-2 py-2 text-left text-xs font-bold text-gray-500 uppercase tracking-wider">
              Address
            </th>
          </tr>
        </thead>
        <tbody className="bg-white">
          {data.map((user, index) => (
            <tr key={index} className="border-b">
              <td className="px-2 py-2 whitespace-nowrap text-xs text-gray-900 font-bold">
                <Link
                  href={`/list/${user.replace(/\s+/g, '-')}`}
                  className="font-medium text-blue-600 underline dark:text-blue-500 hover:no-underline"
                >
                  {user}
                </Link>
              </td>
              <td className="px-2 py-2 whitespace-nowrap uppercase text-xs">
                Delhi
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Users;
