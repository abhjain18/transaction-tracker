import React from 'react';

function TransactionTable({ data }) {
  // Calculate total amount
  const transactions = Object.keys(data).map((key) => {
    const parsedDate = new Date(Number(key));
    const obj = {
      ...data[key],
      date: parsedDate.toLocaleString(),
    };
    return obj;
  });

  const totalAmount = transactions.reduce((acc, cur) => {
    return cur.type === 'paid'
      ? acc - parseFloat(cur.amount)
      : acc + parseFloat(cur.amount);
  }, 0);

  return (
    <div className="w-full overflow-x-auto">
      <table className="w-full min-w-full table-auto">
        <thead className="bg-gray-100">
          <tr>
            <th className="px-2 py-2 text-left text-xs font-bold text-gray-500 uppercase tracking-wider">
              Date
            </th>
            <th className="px-2 py-2 text-left text-xs font-bold text-gray-500 uppercase tracking-wider">
              Type
            </th>
            <th className="px-2 py-2 text-left text-xs font-bold text-gray-500 uppercase tracking-wider">
              Amount
            </th>
          </tr>
        </thead>
        <tbody className="bg-white">
          {transactions.map((transaction) => (
            <tr key={transaction.date} className="border-b">
              <td className="px-2 py-2 whitespace-nowrap text-xs text-gray-900">
                {transaction.date}
              </td>
              <td
                className={`px-2 py-2 whitespace-nowrap uppercase text-xs font-bold ${transaction.type === 'paid' ? 'text-red-500' : 'text-green-500'}`}
              >
                {transaction.type}
              </td>
              <td
                className={`px-2 py-2 whitespace-nowrap text-xs text-gray-900 ${transaction.type === 'paid' ? 'text-red-500' : 'text-green-500'}`}
              >
                {transaction.type === 'paid' ? '-' : '+'}
                {transaction.amount}
              </td>
            </tr>
          ))}
          <tr className="border-t">
            <td className="px-2 py-2"></td>
            <td className="px-2 py-2 text-left text-xs font-bold text-gray-500 uppercase tracking-wider">
              Total
            </td>
            <td
              className={`px-2 py-2 text-sm font-medium ${totalAmount >= 0 ? 'text-green-500' : 'text-red-500'}`}
            >
              {Math.abs(totalAmount).toFixed(2)}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default TransactionTable;
