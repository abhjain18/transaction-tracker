import { useState } from 'react';

export default function CashModal({ saveRecord, onClose }) {
  const [amount, setAmount] = useState('');
  const [transactionType, setTransactionType] = useState('received');

  const handleSubmit = (event) => {
    event.preventDefault();
    saveRecord(amount, transactionType);
  };

  return (
    <div className="fixed inset-0 bg-gray-600 bg-opacity-75 overflow-y-auto h-full w-full flex items-center justify-center px-4">
      <div className="bg-white w-full max-w-md p-6 rounded-lg shadow-lg relative m-4">
        <button
          onClick={onClose}
          className="absolute top-3 right-3 text-gray-600 hover:text-gray-700"
          aria-label="close"
        >
          <svg
            className="w-6 h-6"
            fill="none"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M6 18L18 6M6 6l12 12"
            ></path>
          </svg>
        </button>
        <form onSubmit={handleSubmit}>
          <h2 className="text-lg font-bold mb-4">Add Cash</h2>
          <label
            htmlFor="amount"
            className="block mb-2 text-sm font-medium text-gray-700"
          >
            Amount
          </label>
          <input
            type="number"
            pattern="\d*"
            maxLength={5}
            id="amount"
            name="amount"
            value={amount}
            onChange={(e) => setAmount(e.target.value)}
            className="border-gray-300 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 border rounded-md"
            required
          />
          <fieldset className="mt-6 mb-4">
            <legend className="text-sm font-medium text-gray-700">
              Transaction Type
            </legend>
            <div className="flex items-center mt-2">
              <label className="flex items-center mr-4">
                <input
                  type="radio"
                  name="transactionType"
                  value="received"
                  checked={transactionType === 'received'}
                  onChange={() => setTransactionType('received')}
                  className="w-4 h-4 text-blue-600 border-gray-300 focus:ring-blue-500"
                />
                <span className="ml-2 text-sm text-gray-700">Received</span>
              </label>
              <label className="flex items-center">
                <input
                  type="radio"
                  name="transactionType"
                  value="paid"
                  checked={transactionType === 'paid'}
                  onChange={() => setTransactionType('paid')}
                  className="w-4 h-4 text-blue-600 border-gray-300 focus:ring-blue-500"
                />
                <span className="ml-2 text-sm text-gray-700">Paid</span>
              </label>
            </div>
          </fieldset>
          <button
            type="submit"
            className="mt-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-full"
          >
            Save
          </button>
        </form>
      </div>
    </div>
  );
}
