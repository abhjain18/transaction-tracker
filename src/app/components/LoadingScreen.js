import React from 'react';

const LoadingScreen = () => {
  return (
    <div className="loading-screen">
      <div className="dot"></div>
      <div className="dot"></div>
      <div className="dot"></div>
      <div className="dot"></div>
      <div className="dot"></div>
    </div>
  );
};
export default LoadingScreen;
