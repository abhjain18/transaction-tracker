'use client';

import { useState, useEffect } from 'react';
import CashModal from '@/app/components/CashModal';
import CashTable from '@/app/components/CashTable';
import FloatingButton from '@/app/components/FloatingButton';
import { doc, updateDoc, getDoc } from '@firebase/firestore/lite';
import db from '@/app/libs/firebase';
import { COLLECTION_ID } from '@/app/constants';
import LoadingScreen from '@/app/components/LoadingScreen';

export default function list({ params: { name } }) {
  const [isModalOpen, setModalOpen] = useState(false);
  const [docData, setDocData] = useState({});
  const [isLoading, setLoading] = useState(false);
  const formattedPartyName = name.replace(/-/g, ' ');

  const getDocumentRef = () => {
    return doc(db, COLLECTION_ID, formattedPartyName);
  };

  const getDocumentData = async () => {
    return await getDoc(getDocumentRef());
  };

  useEffect(() => {
    setLoading(true);
    async function fetch() {
      const docSnap = await getDocumentData();
      setLoading(false);
      if (docSnap.exists()) {
        setDocData(docSnap.data());
      }
    }
    fetch();
  }, []);

  const updatePartyRecord = async (amount, transactionType) => {
    await updateDoc(getDocumentRef(), {
      [new Date().getTime()]: {
        type: transactionType,
        amount: amount,
      },
    });
    setModalOpen(false);
    setLoading(true);
    const docSnap = await getDocumentData();
    if (docSnap.exists()) {
      setDocData(docSnap.data());
    }
    setLoading(false);
  };

  if (isLoading) return <LoadingScreen />;

  return (
    <main className="flex flex-col">
      <div className="flex-1 w-full mb-3">
        <div className="flex justify-end">
          <FloatingButton onCashClick={() => setModalOpen(true)} />
        </div>
        {isModalOpen && (
          <CashModal
            saveRecord={updatePartyRecord}
            onClose={() => setModalOpen(false)}
          />
        )}
      </div>
      <div className="flex-1 w-full">
        <h1 className="text-center mb-4 text-1xl font-extrabold text-gray-400 dark:text-white md:text-2xl">
          <span className="text-transparent bg-clip-text bg-gradient-to-r to-emerald-600 from-sky-400">
            {name.replace(/-/g, ' ')}
          </span>{' '}
        </h1>
        <CashTable data={docData} />
      </div>
    </main>
  );
}
// daily-expenses
