'use client';

import React, { useState, useEffect } from 'react';
import Users from '@/app/components/Users';
import PartyModal from '@/app/components/PartyModal';
import LoadingScreen from '@/app/components/LoadingScreen';
import db from '@/app/libs/firebase';
import { collection, getDocs, setDoc, doc } from '@firebase/firestore/lite';
import { COLLECTION_ID } from '@/app/constants';

function Home() {
  const [isModalOpen, setModalOpen] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [userList, setUsersData] = useState([]);

  const getCollection = () => {
    return collection(db, COLLECTION_ID);
  };

  const fetchDocuments = async () => {
    const snapshot = await getDocs(getCollection());
    const userList = [];
    snapshot.forEach((doc) => {
      userList.push(doc.id);
    });
    return userList;
  };

  useEffect(() => {
    async function fetch() {
      setLoading(true);
      const res = await fetchDocuments();
      setLoading(false);
      setUsersData(res);
    }
    fetch();
  }, []);

  const addParty = async (name) => {
    await setDoc(doc(db, COLLECTION_ID, name.trim()), {});
    setModalOpen(false);
    setLoading(true);
    const res = await fetchDocuments();
    setLoading(false);
    setUsersData(res);
  };

  if (isLoading) return <LoadingScreen />;

  return (
    <main className="flex flex-col">
      <div className="flex-1 w-full mb-3">
        <div className="flex justify-end">
          <button
            onClick={() => setModalOpen(true)}
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          >
            Add Party
          </button>
        </div>
        {isModalOpen && (
          <PartyModal
            onSaveHandler={addParty}
            closeModal={() => setModalOpen(false)}
          />
        )}
      </div>
      <div className="flex-1 w-full">
        <Users data={userList} />
      </div>
    </main>
  );
}

export default Home;
