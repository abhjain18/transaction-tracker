// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getFirestore } from 'firebase/firestore/lite';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyAc40eBHo5UqZ-Y3hVKj05bYFHyW7rkvck',
  authDomain: 'daily-expenses-f3cec.firebaseapp.com',
  projectId: 'daily-expenses-f3cec',
  storageBucket: 'daily-expenses-f3cec.appspot.com',
  messagingSenderId: '919693259592',
  appId: '1:919693259592:web:47967a133272e914f2797d',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export default db;
